CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


DROP TABLE IF EXISTS news.item;
DROP TABLE IF EXISTS news.src;
DROP SCHEMA IF EXISTS news;


CREATE SCHEMA news;


CREATE TABLE IF NOT EXISTS
  "news"."src" (
    "id" UUID,
    "designation" VARCHAR NOT NULL,
    "hst" VARCHAR(255) NOT NULL,
    "pth" VARCHAR(255) NOT NULL,
    "secure" BOOLEAN NOT NULL,
    "icon_url" VARCHAR,
    "encodng" VARCHAR(255),
    "created_at" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL,
    "deleted_at" TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY ("id")
  );


CREATE TABLE IF NOT EXISTS
  "news"."item" (
    "id" UUID,
    "title" VARCHAR NOT NULL,
    "link" VARCHAR NOT NULL,
    "description" VARCHAR,
    "media" VARCHAR,
    "mediatype" VARCHAR(255),
    "pub_date" TIMESTAMP WITH TIME ZONE NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL,
    "id_src" UUID REFERENCES "news"."src" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    PRIMARY KEY ("id")
  );
