import * as fs from 'fs';
import * as path from 'path';

import { Sequelize } from 'sequelize';

import { getBaseDir } from '../utils/files.js'

import logger from '../services/logger.js';

const {
    PG_HOST,
    PG_PORT,
    PG_DATABASE,
    PG_USER,
    PG_PASSWORD,
    PG_LOGGING
} = process.env;


const base_dir = getBaseDir();
const sql_base_file = path.join(base_dir, '/scripts/db/0.base_ddl.sql');
const sql_ins_file = path.join(base_dir, '/scripts/db/1.inserts.sql');

const sequelize = new Sequelize(`postgres://${PG_USER}:${PG_PASSWORD}@${PG_HOST}:${PG_PORT}/${PG_DATABASE}`, {
    logging: PG_LOGGING === 'true'
});


// connect
try {
    sequelize.authenticate().then(() => {
        logger.info('Sequelize connected')

        // execute
        try {
            // base sql
            const baseCnt = fs.readFileSync(sql_base_file, 'utf8');
            logger.info(`Read SQL file ${sql_base_file}`);

            sequelize.query(baseCnt).then(() => {
                logger.info('SQL file executed');

                // inserts sql
                const insCnt = fs.readFileSync(sql_ins_file, 'utf8');
                logger.info(`Read SQL file ${sql_ins_file}`);

                sequelize.query(insCnt).then(() => {
                    logger.info('SQL file executed');
                }).catch((error) => {
                    logger.error('Unable to execute SQL file: ', error);
                });
            }).catch((error) => {
                logger.error('Unable to execute SQL file: ', error);
            });
        } catch (error) {
            logger.error('Error reading SQL file: ', error);
        }
    });
} catch (error) {
    logger.error('Unable to connect to the database:', error);
}
