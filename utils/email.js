import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';
import ejs from 'ejs';

import * as path from 'path';

import logger from '../services/logger.js';

const {
    SMTP_HOST,
    SMTP_PORT,
    SMTP_SECURE,
    SMTP_USER,
    SMTP_PASS,
    SMTP_FROM,
    MAIL_TEMPLATE_DIR,
    LOG_LEVEL
} = process.env;

const smtpOptions = {
    host: null,
    port: null,
    tls: {
        rejectUnauthorized: false
    },
    debug: true,
    logger: (LOG_LEVEL == 'trace' || LOG_LEVEL == 'debug') ? true : false
};

let initialized = false;

let transport;
let from;

function initialize() {
    try {
        smtpOptions.host = SMTP_HOST;
        smtpOptions.port = SMTP_PORT;
        smtpOptions.secure = SMTP_SECURE === 'true';

        if (SMTP_USER && SMTP_PASS) {
            smtpOptions.auth = {
                user: SMTP_USER,
                pass: SMTP_PASS
            };
        }

        const smtp = smtpTransport(smtpOptions);

        transport = nodemailer.createTransport(smtp);

        from = SMTP_FROM;

        logger.info('> Email Transport Initialized');

        initialized = true;
    } catch (e) {
        logger.error('> [Error] Email Transport Initialization fail...');
    }
}

export async function sendEmail(email, subject, template, lang, content) {
    if (!initialized) {
        initialize();
    }

    const renderedHtml = await ejs.renderFile(path.join(MAIL_TEMPLATE_DIR, lang, template, 'html.ejs'), content);
    const renderedText = await ejs.renderFile(path.join(MAIL_TEMPLATE_DIR, lang, template, 'text.ejs'), content);

    try {
        logger.info('> Sending email to ' + email);
        // const emailRes = await transport.sendMail({
        //     from,
        //     to: email,
        //     subject,
        //     html: renderedHtml,
        //     text: renderedText
        // });

        if (emailRes.accepted.length) {
            logger.info('> Email sent');
            return true;
        } else {
            return false;
        }
    } catch (e) {
        logger.error('> [Error] Sending email to ' + email + ' fail...', e);
        return false;
    }
}
