import axios from 'axios';
import iconv from 'iconv-lite';

export function get(url, encoding) {
    return new Promise((resolve, reject) => {
        axios.get(url, { responseType: 'arraybuffer', responseEncoding: 'binary' })
            .then(response => {
                if (response.status === 200) {
                    const buffer = Buffer.from(response.data, 'binary');
                    const res = iconv.decode(buffer, encoding);

                    resolve(res);
                } else {
                    reject(`WARNING: Problem making request. Got response status ${response.status}`);
                }
            })
            .catch(err => {
                reject(err);
            });
    });
}
