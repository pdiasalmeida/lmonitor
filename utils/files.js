import { fileURLToPath } from 'url';
import { dirname } from 'path';

export function getBaseDir() {
    const __filename = fileURLToPath(import.meta.url);
    let dirnameArr = dirname(__filename).split('/');

    dirnameArr.pop();

    return dirnameArr.join('/');
}
