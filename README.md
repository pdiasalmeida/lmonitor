### Setup

1. Install dependencies

    ```bash
    yarn install
    ```

2. Create database

    ```bash
    sudo -u postgres createuser -P lmonitor
    sudo -u postgres createdb -O lmonitor lmonitor
    yarn run create:db
    ```

3. Edit configuration

    ```bash
    cp .env.test .env.development
    ```
    Configure accordingly
