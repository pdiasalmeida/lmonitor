import { XMLParser } from 'fast-xml-parser';
import { Op } from 'sequelize';

import logger from '../services/logger.js';

import Source from '../models/news/source.js';
import Item from '../models/news/item.js';

import { get } from '../utils/request.js';

export async function findNews() {
    const options = {
        attributeNamePrefix: "",
        attrNodeName: "attr",
        textNodeName: "#text",
        ignoreAttributes: false,
        allowBooleanAttributes: true,
        parseNodeValue: true,
        parseAttributeValue: true,
        trimValues: true,
        cdataTagName: "__cdata",
        cdataPositionChar: "\\c",
        parseTrueNumberOnly: false,
        arrayMode: false
    };
    const parser = new XMLParser(options);

    Source.findAll().then(sources => {
        sources.forEach(source => {
            logger.info(`Parsing feed ${source.hst}${source.pth}`);

            get(`${source.hst}${source.pth}`, source.encodng)
                .then(result => {
                    const jsonObj = parser.parse(result);

                    if (jsonObj.rss && jsonObj.rss.channel && jsonObj.rss.channel.item) {
                        jsonObj.rss.channel.item.forEach(async item => {
                            const itemEnclosure = item.enclosure ? item.enclosure : {};
                            const media = (typeof itemEnclosure !== "undefined") ?
                                itemEnclosure.url : source.icon_url;
                            const mediatype = (typeof itemEnclosure !== "undefined") ?
                                itemEnclosure.type : "image/" + source.icon_url.split('/').pop().split('.').pop();

                            const existingItem = await Item.findOne({
                                where: {
                                    link: item.link
                                }
                            });

                            if (existingItem) {
                                await existingItem.update({
                                    title: item.title,
                                    link: item.link,
                                    id_src: source.id,
                                    description: item.description,
                                    media,
                                    mediatype,
                                    pub_date: item.pubDate
                                });
                            } else {
                                await Item.create({
                                    title: item.title,
                                    link: item.link,
                                    id_src: source.id,
                                    description: item.description,
                                    media,
                                    mediatype,
                                    pub_date: item.pubDate
                                });
                            }
                        });
                    }
                })
                .catch(err => {
                    logger.error(`Problem getting rss source. ${err}`);
                });
        });
    });
}

export async function pruneNews(limit) {
    logger.info(`Pruning existing news`);
    await Item.destroy({
        where: {
            createdAt: {
                [Op.lt]: limit
            }
        }
    });
    logger.info(`Pruning done`);
}
