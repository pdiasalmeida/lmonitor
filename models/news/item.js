import { DataTypes } from 'sequelize';
import sqz from '../../services/sequelize.js';

import Source from './source.js';

const Item = sqz.define('Item', {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
    },

    title: {
        type: DataTypes.STRING,
        allowNull: false
    },

    link: {
        type: DataTypes.STRING,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
        allowNull: true
    },

    media: {
        type: DataTypes.STRING,
        allowNull: true
    },

    mediatype: {
        type: DataTypes.STRING,
        allowNull: true
    },

    pub_date: {
        type: DataTypes.DATE,
        allowNull: false
    },

    id_src: {
        type: DataTypes.UUID,
        allowNull: true
    },
}, {
    schema: 'news',
    tableName: 'item',
    underscored: true,
    paranoid: false
});

export default Item;

Source.hasMany(Item, { foreignKey: 'id_src' });
Item.belongsTo(Source, { foreignKey: 'id_src', as: 'ItemSource' });
