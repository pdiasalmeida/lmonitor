import { DataTypes } from 'sequelize';
import sqz from '../../services/sequelize.js';

const Source = sqz.define('Source', {
    id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
    },

    designation: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hst: {
        type: DataTypes.STRING,
        allowNull: false
    },

    pth: {
        type: DataTypes.STRING,
        allowNull: false
    },

    secure: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },

    icon_url: {
        type: DataTypes.STRING,
        allowNull: true
    },

    encodng: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, {
    schema: 'news',
    tableName: 'src',
    underscored: true,
    paranoid: true
});

export default Source;
