import { sendEmail } from "./utils/email.js";
import { findNews, pruneNews } from "./src/news.js";

const {
    SMTP_TO,
    NEWS_CUTOFF_DAYS
} = process.env;

let subject = "Automatic monitor";
let template = 'test';
let language = 'pt';
let mailcontent = 'conteúdo de email de teste';

const newsCutOff = new Date();
newsCutOff.setDate(newsCutOff.getDate() - NEWS_CUTOFF_DAYS);

async function run() {
    await pruneNews(newsCutOff);
    await findNews();

    await sendEmail(SMTP_TO, subject, template, language,
        { content: mailcontent }, null, null, null);
}

run();
