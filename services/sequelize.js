import { Sequelize } from 'sequelize';

import logger from './logger.js';

const {
	PG_HOST,
	PG_PORT,
	PG_DATABASE,
	PG_USER,
	PG_PASSWORD,
	PG_LOGGING
} = process.env;

const sequelize = new Sequelize(`postgres://${PG_USER}:${PG_PASSWORD}@${PG_HOST}:${PG_PORT}/${PG_DATABASE}`, {
	logging: PG_LOGGING === 'true'
});

export function getModel(modelName) {
	return sequelize.models[modelName];
}

async function init() {
	try {
		await sequelize.authenticate();
		logger.info('Sequelize connected');
	} catch (error) {
		logger.error('Unable to connect to the database:', error);
	}
}

await init();

export default sequelize;
