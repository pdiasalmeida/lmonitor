import pino from 'pino';

const { LOG_RAW, LOG_LEVEL } = process.env;

const opts = {
  level: LOG_LEVEL || 'info', // ['trace', 'debug', 'info', 'warn', 'error', 'fatal']
  base: false, // hide pid and hostname
  redact: { // remove headers
    paths: ['req.headers.authorization', 'req.cookies', 'req.headers.cookie'],
    censor: '--redact--'
  }
};

if (!LOG_RAW) {
  opts.transport = {
    target: 'pino-http-print',
    options: {
      all: true,
      translateTime: 'SYS:yyyy-mm-dd HH:MM:ss.l',
      relativeUrl: true
    }
  };
}

const loggerOpts = Object.freeze(opts);

export default pino(loggerOpts);
